import time


class Logger:
    def __init__(self, file_path, get_data_point_function, init_message):
        self.file_path = file_path
        file = open(file_path, "w")
        file.write("")
        file.close()

        self.get_data_point = get_data_point_function
        print(init_message)

    def write_line(self):
        line = str(time.time()) + ", " + str(self.get_data_point()) + "\n"
        # print("Write " + line)
        file = open(self.file_path, "a")
        file.write(line)
        file.close()