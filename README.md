# System Usage Logger

## Features:
- Log CPU usage, RAM usage, disk usage, temperatures and more into .log text files
- Generate PDF report with graphs from existing logs

## Usage

### Initial setup

`pip install -r requirements.txt`

### Start logging

`python main.py [logs_folder_path]`

If a folder doesn't exist at the given path, one will be created. Note: this works recursively, so if you specify a tree of non-existing folders, all will be created.

### Generate PDF report

`python report.py [output_pdf_path] [log_path1] [log_path2] ... [log_path_n]`

Pass in .log file paths to only process one file.
Pass in folder paths to process all .log files inside.

Example: `python report.py report.pdf logs1 logs2/cpu0_usage.log logs2/cpu1_usage.log `
