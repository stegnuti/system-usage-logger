from matplotlib import pyplot as plt
import sys
import os
import csv
from datetime import datetime
from matplotlib.backends.backend_pdf import FigureCanvasPdf, PdfPages


def main():
    if len(sys.argv) < 3:
        print("Missing required parameter(s): log folder path OR log file path, output PDF file name")
        return
    
    pdf_name = sys.argv[1]
    if not pdf_name.endswith(".pdf"):
        pdf_name += ".pdf"
    paths = sys.argv[2:]
    graph_paths(pdf_name, paths)


def graph_paths(pdf_name, paths):
    report_pages = PdfPages(pdf_name)

    for path in paths:
        if os.path.isdir(path):
            graph_folder(report_pages, path)
        else:
            graph_file(report_pages, path)
    
    report_pages.close()
    print("Report saved to file " + pdf_name + "!")


def graph_folder(report_pages, logs_folder_path):
    file_paths = [os.path.join(logs_folder_path, file_name) for file_name in os.listdir(logs_folder_path)]

    for file_path in file_paths:
        graph_file(report_pages, file_path)


def graph_file(report_pages, file_path):
    if not file_path.endswith(".log"):
        return

    filename = file_path.split("/")[-1]

    print("Graphing " + filename + "...", end="")
    x, y = read_log(file_path)
    label = filename.split(".")[0]

    figure = plt.figure(clear=True)
    plt.plot(x, y)
    plt.ylabel(label)
    plt.gcf().autofmt_xdate()

    canvas = FigureCanvasPdf(figure)
    canvas.print_figure(report_pages)

    plt.close()
    print(" Done!")


def read_log(path):
    file = open(path, "r")
    reader = csv.reader(file, delimiter=",")

    x = []
    y = []
    for row in reader:
        timestamp = float(row[0])
        timestamp = datetime.utcfromtimestamp(timestamp)
        value = float(row[1])
        x.append(timestamp)
        y.append(value)

    return x, y


if __name__ == "__main__":
    main()